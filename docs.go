// Package errsql provides a wrapper for a [database/sql/driver.Driver] which
// enables custom error handling and other hooks.
package errsql
