module gitlab.com/flimzy/errsql

go 1.21.3

require (
	github.com/mattn/go-sqlite3 v1.14.19
	github.com/pkg/errors v0.9.1
)
